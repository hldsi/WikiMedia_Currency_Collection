# Python scripts automating bulk download of American Currency Collection image files and Wikimedia Commons screenshots

## Downloading image files for the American Currency Collection, Baker Library at Harvard Business School, using a metadata spreadsheet from JSTOR Forum

This program utilizes a metadata CSV file titled "jstor_forum_metadata"
as well as a folder titled "collection_images" (both the CSV and the folder should be in
the same directory as that from which you are executing the code).
To demonstrate that this program works as intended, the "collection_images" folder
available in this subdirectory includes all 1400 image files.

Please note that the metadata included with this code in GitLab is likely to change 
over time, meaning that this program will not work in perpetuity. The intent of posting 
this code and the corresponding metadata CSV file to GitLab is to document the first script and 
spreadsheet file used successfully by the Harvard Library Digital Strategies and Innovation interns 
to acquire 1400 collection images on our local machines in summer 2020.

This program might require some monitoring by the person using it. 
Occasional lapses in the process of downloading do not necessitate deleting the
download folder and starting over. Instead, simply check the most recently downloaded file
for incomplete downloading, delete that file if necessary, then close and re-open your command prompt
before running the program again.


## Automated capture and download of Wikimedia Commons screenshots for the American Currency Collection, Baker Library at Harvard Business School

This program is designed for use specifically with a Google Chrome driver.
Before attempting to use this program, be sure to have the appropriate driver installed and 
located in the same directory as the program code and the "wikimedia_url_prep_for_script" CSV file.

To demonstrate that this program works as intended, the "wikimedia_screenshots" folder available
in this subdirectory includes all 1400 screenshots from the Wikimedia Commons collection.

The following web resources were helpful in developing the screenshot automation code:

"Take Webpage Screenshot with Python Selenium," 
https://pythonbasics.org/selenium-screenshot/ (accessed August 7, 2020)

"Selenium with Python, 1. Installation,"
https://selenium-python.readthedocs.io/installation.html (accessed August 7, 2020)


## Before using either program

Be sure to have the necessary Python modules installed via PIP.
These modules are viewable in the import statements at the top of the code
of both programs.

