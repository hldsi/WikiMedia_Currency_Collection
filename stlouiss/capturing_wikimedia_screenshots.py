

# AUTOMATED CAPTURE AND DOWNLOAD OF WIKIMEDIA SCREENSHOTS FOR AMERICAN CURRENCY COLLECTION,
# BAKER LIBRARY (HARVARD BUSINESS SCHOOL).

# READ_ME: This program is designed for use specifically with a Google Chrome driver.
# Before attempting to use this program, be sure to have the appropriate driver installed and 
# located in the same directory as the program code and the "wikimedia_url_prep_for_script" CSV file.

# The following web resources were helpful in developing the code below:

# "Take Webpage Screenshot with Python Selenium," 
# https://pythonbasics.org/selenium-screenshot/ (accessed August 7, 2020)

# "Selenium with Python, 1. Installation,"
# https://selenium-python.readthedocs.io/installation.html (accessed August 7, 2020)


import csv
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def read_csv(filename):
    """
    This function has been taken directly from the slides of
    Lecture 17 from Professor Anthony Whyte's SI 506 class (Python I)
    at the University of Michigan School of Information, Fall 2019 semester.

    Retrieves data from a CSV file.

    Parameters
    ----------
    filename: str
        The name of the CSV file.

    Returns
    ----------
    data: list
        Data from each row in the CSV file.

    """

    data = []

    with open(filename, 'r', encoding='utf-8-sig') as file_obj:
        reader = csv.reader(file_obj, delimiter=',')
        for row in reader:
            data.append(row)

    return data



if __name__ == "__main__":

    print("\n\n\n\n\n")
    print("Running program. Check your file directory for screenshots as they are captured and downloaded.")
    print("\n\n\n\n\n")

    wikimedia_url_list = []

    wikimedia_data = read_csv(filename="wikimedia_url_prep_for_script.csv")

    for data_row in wikimedia_data:
        data_row[1] = data_row[1].replace(" ", "_")
        wikimedia_url = data_row[0] + data_row[1]
        wikimedia_url = wikimedia_url.replace(" ", "")
        wikimedia_url_list.append(wikimedia_url)


    options = webdriver.ChromeOptions()
    options.headless = True
    driver = webdriver.Chrome(options=options)

    i = 0

    for url in wikimedia_url_list:

        i+=1

        driver.get(url)
        S = lambda X: driver.execute_script('return document.body.parentNode.scroll'+X)
        driver.set_window_size(S('Width'),S('Height')) # May need manual adjustment                                                                                                                
        driver.find_element_by_tag_name('body').screenshot(f"wikimedia_screenshot_{i}.png")
        print(f"Captured and downloaded screenshot{i}")

        if i == 1401:

            print('\n\n\n\n\n')
            print('--------------------------------------------------')
            print("END PROGRAM.")
            print('--------------------------------------------------')
            print('\n\n\n\n\n')

            driver.quit()

        else:
            continue
      