

# DOWNLOADING IMAGE FILES FOR THE AMERICAN CURRENCY COLLECTION (BAKER LIBRARY AT HARVARD)
# USING A METADATA SPREADSHEET FOR THE COLLECTION FROM JSTOR FORUM.

# READ_ME: This program utilizes a metadata CSV file titled "jstor_forum_metadata"
# as well as a folder titled "collection_images" (both the CSV and the folder should be in
# the same directory as that from which you are executing the code).

# READ_ME: Please note that the metadata included with this code in GitLab is likely to change 
# over time, meaning that this program will not work in perpetuity. The intent of posting 
# this code and the corresponding metadata CSV file to GitLab is to document the first script and 
# spreadsheet file used successfully by the Harvard Library Digital Strategies and Innovation interns 
# to acquire 1400 collection images on our local machines in summer 2020.

# READ_ME: This program might require some monitoring by the person using it. 
# Occasional lapses in the process of downloading do not necessitate deleting the
# download folder and starting over. Instead, simply check the most recently downloaded file
# for incomplete downloading, delete that file if necessary, then close and re-open your command prompt
# before running the program again.


import csv
import urllib.request
import os


def read_csv(filename):
    """
    This function has been taken directly from the slides of
    Lecture 17 from Professor Anthony Whyte's SI 506 class (Python I)
    at the University of Michigan School of Information, Fall 2019 semester.

    Retrieves data from a CSV file.

    Parameters
    ----------
    filename: str
        The name of the CSV file.

    Returns
    ----------
    data: list
        Data from each row in the CSV file.

    """

    data = []

    with open(filename, 'r', encoding='utf-8-sig') as file_obj:
        reader = csv.reader(file_obj, delimiter=',')
        for row in reader:
            data.append(row)

    return data



def download_jpg(url, file_path, file_name):
    """
    Developed this function with guidance from a YouTube video titled 
    'Python 3 Tutorial #29: Downloading Images,' by NetNinja. 
    Posted to YouTube on July 24, 2017. Accessed July 6, 2020.
    https://www.youtube.com/watch?v=2Rf01wfbQLk

    Parameters
    ----------
    url: str
        The URL for the JPG file.

    file_path: str
        The directory/folder where the JPG should be saved.

    file_name: str
        The name given to the JPG file when it is downloaded.

    Returns
    ----------
    None

    """

    full_path = file_path + file_name
    urllib.request.urlretrieve(url, full_path)
    print(f"{file_name} downloaded successfully!")




if __name__ == "__main__":

    
#    Utilized the following Stack Overflow discussion to write the code below (accessed July 6, 2020):
#    https://stackoverflow.com/questions/8286352/how-to-save-an-image-locally-using-python-whose-url-address-i-already-know

    urn_list = []
    url_list = []
    ssid_list = []

    metadata_dict = {}

    currency_data = read_csv(filename="jstor_forum_metadata.csv")

    for item_record in currency_data:

        if item_record[0].isnumeric() == True:
            ssid_list.append(item_record[0])

        if "drs" in str(item_record[1]):
            urn_list.append(item_record[1])

    for urn in urn_list:
        url = urn.replace("drs:", "https://nrs.harvard.edu/")
        url_list.append(url)

    i=0

    for ssid in ssid_list:
        metadata_dict[ssid_list[i]]=url_list[i]
        i+=1


# Source for help constructing loop below: Vuyisile Ndlovu for Real Python, "Working with Files in Python,"
# https://realpython.com/working-with-files-in-python/ (accessed July 6, 2020).

    for ssid, url_item in metadata_dict.items():
        url_item = str(url_item)
        file_name = f"ssid_{str(ssid)}.jpg"
        image_file = f"collection_images/{file_name}"
        if os.path.isfile(image_file):
            continue
        else:
            download_jpg(url_item, "collection_images/", file_name)



