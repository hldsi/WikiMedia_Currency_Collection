# Repository for the Harvard Library CURIOSity / Wikimedia Upload Pilot Project

This repository includes subdirectories of the summer 2020 Harvard Library Digital Strategies and Innovation (DSI) interns:

- irmafrat, Irmarie Fraticelli-Rodríguez
        (Main codebase that automatically interfaces Harvard LibraryCloud API metadata into a file prepared for uploading images into Wikimedia Commons using Wikipedia's Pattypan tool). [See More](https://gitlab.com/hldsi/WikiMedia_Currency_Collection/-/tree/master/irmafrat)

- leclouse, Leigh Clouse
        (Additional project workflow documentation, including vital contributions on selecting the proper rights statement). [See More](https://gitlab.com/hldsi/WikiMedia_Currency_Collection/-/tree/master/leclouse)

- stlouiss, Scott St. Louis
        (Two Python scripts respectively for [1] automating bulk download of American Currency Collection images, 1400 in total, from an associated JSTOR Forum metadata spreadsheet provided by the Harvard Library DSI team; and [2] automating capture and download of 1400 corresponding screenshots from Wikimedia Commons). [See More](https://gitlab.com/hldsi/WikiMedia_Currency_Collection/-/tree/master/stlouiss)

The main purpose of this repository is to provide a common space for storing and documenting Python programs made for this project.

Additionally, this repository provides a space to present robust workflow documentation for future DSI interns, future Harvard-Wikimedia upload projects, and future rights assessment strategies.

## Acknowledgments

We extend our deepest thanks to Lena Denis and Wendy Gogel for guiding us through the development and implementation of this project, as well as to Kyle Courtney for empowering us to move forward after reviewing our work on rights statements, and to Christine Riggle for sharing with us her expertise on the American Currency Collection at Baker Library, Harvard Business School. Our warm appreciation is due also to the Digital Strategies and Innovation team at Harvard Library -- particularly Vanessa Venti and Claire DeMarco -- for supporting us at each step along the way. Thank you all for a wonderful learning opportunity this summer!

## Request to Future Project Contributors

We ask kindly that future contributors to this repository at Harvard Library preserve the versions we developed over the summer of 2020. 

Such preservation is very important for our long-term professional documentation needs, as we might seek to share with others the repository we developed in order to display the work we did, as well as what we learned while doing it.

We recommend the use of [branching](https://nvie.com/posts/a-successful-git-branching-model/) to ensure that our contributions are preserved in their original form, though other possibilities might prove more effective.

Thank you!

(Irma, Leigh, and Scott)
