# Background

The [No Copyright - United States rights statement](https://rightsstatements.org/page/NoC-US/1.0/?language=en) from RightsStatement.org was determined to be the most suitable for inclusion in the Wikimedia item metadata by the team, a choice that was approved by Kyle Courtney, Copyright Advisor for Harvard University. In his recommendation, Courtney emphasized that no copyright protection is available for any work of the U.S. government, which includes currency.

As Wikimedia requires the use of one of their licensing tag templates to be added to each item to avoid them from being deleted, the team decided that the [PD-US tag](https://commons.wikimedia.org/wiki/Template:PD-US) included the most appropriate messaging in comparison to the other United States-specific licensing tags. This decision was also approved by Kyle Courtney.

# Copyright and Rights Statement Resources Used

The following links were used in the process to determine what is permissible for upload on Wikimedia as well as to discern what kind of rights language should accompany each American Currency Collection item:

- [Wikimedia rights info and CURIOSity Collections document](https://docs.google.com/document/d/1tK0BUCYFQkS939Wep-fp2smHCetW9nopgBSP4K_cqQc/edit?usp=sharing), which compiled info from the following Wikimedia sources:

    - [Commons:Licensing](https://commons.wikimedia.org/wiki/Commons:Licensing) (discussion of acceptable licenses on Wikimedia)

    - [Help:Public Domain](https://commons.wikimedia.org/wiki/Help:Public_domain) (discussion of what is considered in the public domain)
    
    - [Commons:Copyright tags](https://commons.wikimedia.org/wiki/Commons:Copyright_tags) (discussion of required licensing tags --   must add one to  item avoid deletion) and [Template:PD-US](https://commons.wikimedia.org/wiki/Template:PD-US) (licensing tag applied to collection items)
        
- [Commons:Currency](https://commons.wikimedia.org/wiki/Commons:Currency) (rights information specifically about currency)

- [RightsStatement.org](https://rightsstatements.org/en/) (lists 12 rights statements for online cultural heritage) and [No Copyright - United States statement](https://rightsstatements.org/page/NoC-US/1.0/?language=en) (statement used in permissions metadata field)    
      
- [Creative Commons Public Domain Mark](https://creativecommons.org/share-your-work/public-domain/pdm/) (information and guidelines about the Public Domain Mark)
