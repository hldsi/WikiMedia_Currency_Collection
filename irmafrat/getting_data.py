# WikiMedia project: Uses a URN to obtain JSON files of each objects.
# The JSON files are being stored as cache.
import csv
import requests
import os.path
from time import time
from irmacache import Cache
import harvard_item_class as hic
import harvard_item_class_xml as hic_xml

# Set output file
OUTPUT_FILE = "wikimedia_bash_upload.csv"

# Set local directory. Use the path where images are located.
SAVE_DIR = "/home/irma/Documents/dsi_currency_imgs_v2/"

# Set the local file where the URN's are listed. (Recomended: transform the JSTOR FORUM xlxs in a CSV file.)
DSI_JSTOR_CSV= "American Currency collection_money-JSTOR.csv"

# Set local Harvard Library Clode API metadata cache file (full path or relative)
LOCAL_HLC_API_CACHE = 'currency_cache_xml.json'

# Set the WikiMedia Categories for the collections that you are uploading. 
# Categories are separated by ";" no spaces.
WIKIMEDIA_CATEGORIES = "Harvard University;Harvard Business School;Harvard University Library;Harvard CURIOSity Collections;Baker Library (Harvard Business School);American Currency Collection;Numismatics in United States of America"

# Set permission statement for the collection. 
PERMISION_STATEMENT = "The organization that has made the Item available believes that the Item is in the Public Domain under the laws of the United States, but a determination was not made as to its copyright status under the copyright laws of other countries. The Item may not be in the Public Domain under the laws of other countries. Please refer to the organization that has made the Item available for more information (URI: https://rightsstatements.org/page/NoC-US/1.0/?language=en)"


cache = Cache(filename=LOCAL_HLC_API_CACHE)


# Returns a list of dictionaries. This functions applies to this specific project. 
# The input 'filename' should be of a csv file containing a column named "Filename" where 
# image urn's are located. Defaults to DSI_JSTOR_CSV.
def data_csv(filename=DSI_JSTOR_CSV):
    out = []
    with open(filename, 'r', encoding="utf-8") as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row_dict in csv_reader:
            out.append(row_dict.copy())
    return out


# Returns the urn according to the LibraryCloudAPI. It uses a JSTOR FORUM METADATA XLSX.
# drs:urn-3:HBS.Baker.AC:1142292
# urn-3:HBS.Baker.AC:1142292
def clean_urn(dirty_urn):
    return dirty_urn[dirty_urn.find("urn-3:"):]


# Search in API using the URN and returning a XML of the item. The response will be saved on a cache file.
def xml_search_library_cloud(urn: str):
    base_url = "https://api.lib.harvard.edu/v2/items"
    params = {'urn': urn}
    response = cache.gw_xml(base_url, params, wait_time=15, number_tries=2)
    if type(response) is str:
        print("XML not found")
        return None
    return response


def main():
    image_absolute_save_dir = SAVE_DIR
    if not os.path.isdir(image_absolute_save_dir):
        os.makedirs(image_absolute_save_dir)

    image_filename_metadata = OUTPUT_FILE

    dict_list = data_csv()

    total = 0
    # This columns match the default PattyPan template.
    pattypan_columns = ["path", "name", "author", "title", "description", "date", "medium", "dimensions",
                        "institution", "department", "inscriptions", "notes", "accession_number", "source", "permission", "categories"]
    csv_header = ",".join(pattypan_columns).replace('"', '""')
    handler = open(image_filename_metadata, "w")
    handler.write(csv_header + "\n")

    
    for csv_dict in dict_list:
        total += 1
        print(f"Processing image #{total}")
        urn = clean_urn(csv_dict['Filename'])
        api_dom = xml_search_library_cloud(urn)
        if api_dom is None:
            print("No API item found.")
            continue
        item = hic_xml.CurrencyItem(
            api_dom, csv_dict, image_relative_save_dir=image_absolute_save_dir)

        item.permission = PERMISION_STATEMENT
        item.categories = WIKIMEDIA_CATEGORIES
        print(f"File at: {item.file_path()}")
        csv_row = item.wikimedia_csv_currency(column_list=pattypan_columns)
        handler.write(csv_row + "\n")
    handler.close()


if __name__ == "__main__":
    start = time()
    main()
    end = time()
    print("Executed in: %s seconds" % (end-start))

