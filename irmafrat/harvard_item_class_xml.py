import requests
from time import sleep
import hashlib
import bs4
import os
import imghdr


class HarvardItem:
    max_retry=20
    permission = None 
    metadata_list=[
        "title","source","permission"
    ]

        
    
    def __init__(self, api_dom:bs4.BeautifulSoup, image_absolute_save_dir="./image_repo/"):
        self.image_save_dir = image_absolute_save_dir
        if not os.path.isdir(self.image_save_dir):
            os.makedirs(self.image_save_dir)
        self.api_dom = api_dom
        self.categories= None
        self.urn = self.get_urn()
        self.url: str = self.get_url()
        self.deep_link = None
        self.title = self.get_title()
        self.media_binary = None
        self.known_extension = None
        self.embbed_urls = self.get_embbed_urls()
        self.source = self.get_source()
        self.sha1 = self.calculate_sha1()

    def calculate_sha1(self):
        if self.media_binary is None:
            self.download_media()
        sha1 = hashlib.sha1(self.media_binary).hexdigest()

        return sha1

    def get_urn(self):
        return self.api_dom.pagination.query.string[4:]
    
    def get_url(self):
        try:
            url = self.api_dom.find("mods:relateditem", {"type":"constituent"}).find("mods:url", {"access":"raw object"}).string
        except:
            try:
                url = self.api_dom.find("harvarddrs:filedeliveryurl").string
            except:
                return f"No URL found"
        return url
    
    def get_source(self):
        source = ""
        for key, value in self.embbed_urls.items():
            source += f"{key}: {value} - "
        return source[:-3]
    
    def get_deep_link(self, sleep_time=30):
        deep_url = None
        if self.url == "No URL found":
            print("Required URL to get deep link.")
            return deep_url
        response= requests.head(self.url)
        total_tries= 0
        while not response.ok:
            total_tries +=1
            print(f"Retry #{total_tries}")
            sleep(sleep_time)
            response= requests.head(self.url)
            if total_tries > self.max_retry:
                break

        if response.ok:
           
            deep_url = response.headers['Location']
            return deep_url
        return deep_url

    def wikimedia_csv(self, column_list=None):
        if self.permission is None:
            print("Permissions are required before generating CSV output.")
            return None
        csv_row=""
        if column_list is None:
            column_list = self.metadata_list
        
        for metadata in column_list:
            if "path" == metadata:
                csv_row += '"' + self.file_path() +'",'
                continue
            elif "name" == metadata:
                csv_row += '"' + self.filename() +'",'
                continue
            else:
                value = self.__getattribute__(metadata)
            if value is not None:
                csv_row += '"' + value.replace('"', '""') + '",'
            else:
                csv_row += ','
        return csv_row[:-1]

    def wikimedia_source(self):
        if self.deep_link is None:
            self.deep_link = self.get_deep_link()
        return f"URL: {self.url} \nDeep Link: {self.deep_link}"

    def get_title(self):
        return self.api_dom.find("mods:titleinfo").string.replace(", ", "_")
    
    def download_media(self, sleep_time=30):
        if self.media_binary is None:
            print(f"Trying pulling '{self.filename()}' from {self.image_save_dir}.")
            self.restore_media()
        if self.media_binary is None:
            print(f"Could not restore image. Proceeding to download.")          
            self.media_binary = self.get_content(sleep_time)
            self.save()
        else:
            print("Sourced binary from Storage.")

    
    def save(self):
        with open(self.file_path(), "wb") as handler:
            handler.write(self.media_binary)
    
    def extension(self):
        if self.known_extension is not None:
            return self.known_extension
        else:
            extension = ""
            if self.media_binary is not None:
                tmp_fn = "tmp_img"
                with open(tmp_fn, "wb") as handler:
                    handler.write(self.media_binary)
                extension = imghdr.what(tmp_fn)
                os.remove(tmp_fn)
            if extension != "":
                self.known_extension = extension
            return extension

    def restore_file_name(self):
        files_in_destination = os.listdir(self.image_save_dir)
        for f in files_in_destination:
            if self.urn.replace(":","_") in f:
                return f
        return None
        
    
    def restore_media(self):
        try:
            ext=None
            storage_full_path = os.path.join(self.image_save_dir, str(self.restore_file_name()))
            ext = imghdr.what(str(storage_full_path))
            print(f"File path    = '{self.file_path()}'.")
            print(f"Storage path = '{storage_full_path}'.")
            if ext != "" or ext is None:
                self.known_extension = ext
            else:
                print("no file match")
            print(f"Ext found = *.{ext}")
            if os.path.isfile(storage_full_path):
                with open(self.file_path(), "rb") as handler:
                    self.media_binary = handler.read()
            else:
                print("file not found")
                self.media_binary = None
        except:
            self.media_binary = None

    def get_content(self, sleep_time=30):
        if self.deep_link is None:
            self.deep_link = self.get_deep_link()
        if self.deep_link == "Required URL to get deep link.":
            print("Required deep link to download media binary.")
            return None
        response= requests.get(self.deep_link)
        total_tries= 0
        while not response.ok:
            total_tries +=1
            print(f"Retry #{total_tries}")
            sleep(sleep_time)
            response= requests.get(self.deep_link)
            if total_tries > self.max_retry:
                break

        if response.ok:
            media_binary = response.content
            return media_binary
        else: 
            print("Could not download media")
            return None
    
    def set_permission(self, permission):
        self.permission = permission


    def get_embbed_urls(self):
        urls= {}
        try:
            hollis_images = self.api_dom.find("mods:relateditem", {"othertype":"HOLLIS Images record"}).find("mods:url").string
            urls.update({"Harvard Hollis Images": hollis_images})
        except:
            print ("No HOLLIS Image record found")
        urls_context = self.api_dom.findAll("mods:url",{"access":"object in context"})
        for mods_url in urls_context:
            try:
                url = mods_url.string
                key = mods_url.attrs["displaylabel"]
                if "curiosity" in url: 
                    key = "CURIOSity - " + key 
                urls.update({key:url})
            except:
                print("No displaylabel found")
        return urls

    def filename(self, with_extension=True):
        output = f"{self.title.replace('/','-').replace('?','_')}_{self.urn.replace(':','_')}"
        if with_extension and self.extension() != "":
            output = output + "." + self.extension()
        return output

    def file_path(self):
        return str(os.path.join(self.image_save_dir, self.filename()))


class CurrencyItem(HarvardItem):
    institution = "[[wikidata:Q49126|Harvard Business School]]" # HBS. WikiData institution item identifier 
    department = "[[wikidata:Q42377658|Baker Library]]" # Baker. WikiData institution item identifier 
    metadata_list_currency=["author","title","description","date","medium","dimensions","institution","department","inscriptions","notes","accession_number", "deep_link", "sha1"]
    pattypan_columns=["path", "name", "author", "title", "description", "date", "medium", "dimensions", "institution", "department", "inscriptions", "notes", "accession_number", "source", "permission", "categories"]
    def __init__(self,api_dom:bs4.BeautifulSoup, csv_dict:dict, image_relative_save_dir="./image_repo/"):
        super().__init__(api_dom, image_relative_save_dir)
        self.csv_dict = csv_dict
        self.author = self.get_author() # Data is in the CSV
        self.description = self.get_description()
        self.dimensions = self.get_dimensions()
        self.medium = self.get_medium() # Data is in the CSV
        self.date = self.get_date()
        self.inscriptions = self.get_inscriptions()
        self.notes = self.get_notes()
        self.accession_number = self.get_accession_number()

    def wikimedia_csv_currency(self, column_list=None):
        csv_row=""
        if column_list is None:
            csv_row = super().wikimedia_csv() + ","
            column_list = self.metadata_list_currency
        for metadata in column_list:
            if "path" == metadata:
                csv_row += '"' + self.file_path() +'",'
                continue
            elif "name" == metadata:
                csv_row += '"' + self.filename() +'",'
                continue
            else:
                value = self.__getattribute__(metadata)
            if value is not None:
                if metadata == "date" and type(value) is dict:
                    value = value["date1"]
                csv_row += '"' + value.replace('"', '""') + '",'
            else:
                csv_row += ','
        return csv_row[:-1]
    
    def get_csv_header(self):
        metadata_column_list = self.metadata_list + self.metadata_list_currency
        csv_header= ""
        for metadata in metadata_column_list[:-1]:
            csv_header += metadata + ","
        csv_header+= metadata_column_list[-1] + "\n"
        return csv_header

    def get_title(self):
        title = super().get_title()
        verso_recto = self.get_verso_recto()
        if verso_recto is None:
            return title
        else:
            return f"{verso_recto.strip(':')}_{title}"
    
    def get_verso_recto(self):
        try:
            return self.api_dom.find("mods:relateditem", {"type":"constituent"}).find("mods:title").string
        except:
            return None
    
    def get_author(self):
        return self.csv_dict["Creator[33420]"]

    def get_description(self):
        try:
            return self.api_dom.find("mods:abstract").string
        except:
            return None

    def get_dimensions(self):
        try: 
            return self.api_dom.find("mods:physicaldescription").find("mods:extent").string
        except:
            return None

    def get_medium(self):
        csv_medium = self.csv_dict["Materials/Techniques[33429]"]
        return f"{csv_medium}"

    def get_date(self):
        dates = self.api_dom.findAll("mods:datecreated")
        for date in dates:
            if date.attrs.get("point", None) == "start":
                return date.string
        
        for date in dates:
            if date.attrs.get("point", None) == "end":
                return date.string
        
        for date in dates:
            return date.string
        
        return None


    def get_inscriptions(self):
        try:
            notes= self.api_dom.findAll("mods:note")
            for note in notes:
                if "inscription" in note.string.lower():
                    return note.string
            return None
        except:
            return None

    def get_notes(self):
        try:
            return "".join(self.api_dom.find("harvarddrs:metslabel").string.split(" Development of American Capitalism--Money and banking."))
        except:
            return None

    def get_accession_number(self):
        record_id= self.api_dom.find("mods:recordidentifier")
        return f"{record_id.attrs['source']} {record_id.string}"
      

    