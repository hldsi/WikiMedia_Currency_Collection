# Wikimedia Commons Upload Pilot Project: American Currency Collection

Table of contents

- [Wikimedia Commons Upload Pilot Project: American Currency Collection](#wikimedia-commons-upload-pilot-project-american-currency-collection)
  - [Background](#background)
  - [Purpose](#purpose)
  - [Suggested workflow](#suggested-workflow)
  - [Results](#results)
  - [Data Provenance](#data-provenance)
  - [Data Access and Manipulation](#data-access-and-manipulation)
      - [Metadata Cache](#metadata-cache)
      - [Image Cache](#image-cache)
  - [Description of the record](#description-of-the-record)
    - [Rights Statements](#rights-statements)
  - [Packages Used](#packages-used)
  - [Program Description](#program-description)
    - [Version 1.0 OLD VERSION (Access Harvard LibraryCloud API using JSON)](#version-10-old-version-access-harvard-library-cloud-api-using-json)
    - [Version 2.0 CURRENT VERSION (Access Harvard LibraryCloud API using XML)](#version-20-current-version-access-harvard-library-cloud-api-using-xml)
- [How to use](#how-to-use)
  - [Prepare your environment](#prepare-your-environment)
  - [Set script global variables](#set-script-global-variables)
      - [Windows](#windows)
      - [Linux and Macs](#linux-and-macs)
  - [Execute](#execute)
    - [Pattypan output file preparation](#pattypan-output-file-preparation)
      - [Pattypan template](#pattypan-template)
    - [Run it for other collections](#run-it-for-other-collections)
      - [For other developers](#for-other-developers)
- [Further development](#further-development)
  - [Suggestion for Next Step: Google Analytics integration for tracking Wikimedia impact](#suggestion-for-next-step-google-analytics-integration-for-tracking-wikimedia-impact)
    - [Objective](#objective)
    - [Methods](#methods)
    - [Metrics](#metrics)
  - [Suggestion for Next Step: Consider integrating archival finding aids with Wikidata](#suggestion-for-next-step-consider-integrating-archival-finding-aids-with-wikidata)
  - [Suggestion for Next Step: Consider adding a Wikidata item for the American Currency Collection as a whole](#suggestion-for-next-step-consider-adding-a-wikidata-item-for-the-american-currency-collection-as-a-whole)

#

## Background

During the summer of 2020, three Harvard Library Digital Strategies and Innovation (DSI) interns worked to identify a low-risk collection ([American Currency Collection](http://nrs.harvard.edu/urn-3:hul.eresource:americancurrency)) with which to pilot a workflow for uploading digitized Harvard Library collections to Wikimedia Commons to increase overall access. Making digital images available via Wikimedia makes finding digitized images easier. Interviews with image researchers by Harvard Library revealed that most of them discover images online via Google Image search, and they often use Wikimedia to link to the original repository for context, related images, and further searching. [Wikimedia Commons](https://commons.wikimedia.org/wiki/Commons:Welcome) is a platform in the Wikimedia family of services, allowing users to share media files for educational purposes.

## Purpose

The purpose of this code is develop a program that allows the user to download data from the Harvard LibraryCloud API and prepare it for upload to Wikimedia Commons using the Pattypan tool to perform the final upload.

## Suggested workflow

![Suggested workflow for the DSI & Harvard Library Team](./Suggested_workflow_final_Page 2.jpeg)

[See image](https://drive.google.com/file/d/14q0AW3-1PW2oRQWLAVkvnbPh9n6t3pPM/view?usp=sharing)


## Results

You can access the uploaded image resources as public domain media inside the [Wikimedia American Currency Collection](https://commons.wikimedia.org/wiki/Category:American_Currency_Collection). A total of 1,400 images were uploaded with metadata.

As part of the project three new categories were added to Wikimedia Commons to better describe the collection and to group together the items uploaded. These include: 

- [Harvard CURIOSity Collections](https://commons.wikimedia.org/wiki/Category:Harvard_CURIOSity_Collections)
- [American Currency Collection](https://commons.wikimedia.org/wiki/Category:American_Currency_Collection)
- [Numismatics in United States of America](https://commons.wikimedia.org/wiki/Category:Numismatics_in_United_States_of_America)

#

## Data Provenance

| Source | DATA                                | FORMAT      |
|-----| -------------------------------------- | ----------- |
| Baker Library, Special Collections (Harvard Business School)| JSTOR Forum American Currency Metadata | .xlsx / CSV |
| Harvard LibraryCloud API | Item Metadata | JSON / XML  |
| [Harvard IDS](https://wiki.harvard.edu/confluence/pages/viewpage.action?pageId=230000245) | Image Files | jpeg/png/tiff/...|

## Data Access and Manipulation

The Harvard Library DSI team was able to help us gather the metadata for this project. We converted the JSTOR Forum medatata file (.xlsx) to a CSV file. JSTOR Forum includes source catalog item metadata, whereas LibraryCloud is the JSTOR Forum metadata transformed into MODS.

The [Harvard LibraryCloud API](https://wiki.harvard.edu/confluence/display/LibraryStaffDoc/LibraryCloud) is used to request additional information about objects in the American Currency Collection. The goal is to match the API data with metadata provided on the JSTOR Forum spreadsheet to generate enriched records for Wikimedia Commons.

We studied the Wikimedia Commons requirements for filenaming conventions, file uploading and metadata crosswalking between the [WikiMedia artwork template](https://commons.wikimedia.org/wiki/Template:Artwork), the JSTOR Forum metadata, and the Harvard LibraryCloud API data.

#### Metadata Cache

This program generates a cache file in XML or JSON formats to store the data of the items requested. The program uses a cache to keep a copy of the requested metadata. This reduces the communication overhead when working remotely. Cache constructed out of a dictionary stored locally in a JSON file. Both JSON and XML data can be stored in this cache.

Current cache implementation tries to return a 'soup' (from BeautifulSoup) that is later used to construct the item object (both CurrencyItem and HarvardItem).

In this program the cache also manages potential errors that can happen when it communicates with the Harvard LibraryCloud API, and waits the designated time to reestablish connection. This allows the program to run more efficiently, reduces probability of data loss, and allows for unsupervised execution. Large image sets may take a long time to process the first time.

#### Image Cache

The HarvardItem class developed in this project stores copies of the images processed in a local folder (default=`./image_repo/`) that can be customized. This folder can be used by the class for later reimplementation without requiring downloading the image again.

## Description of the record

The program generated a CSV file containing metadata for 1400 images from the American Currency Collection ([see metadata spreadsheet](https://docs.google.com/spreadsheets/d/1lLXDB0SPvfl6T6b8-Qpcfp51ySQcFczlRz1MYZ87sR4/edit?usp=sharing)). 

We also selected an appropriate rights statement for the items from RightsStatements.org and received authorization to use this rights statement from copyright experts affiliated with Harvard Library.

Metadata fields:

| Field            | Usage                                                                                                  |
| ---------------- | ------------------------------------------------------------------------------------------------------ |
| path             | Local drive file location                                                                              |
| name             | Filename proposed for commons.                                                                         |
| title            | A combination of the side of the currency (verso or recto), description, and date                      |
| source           | Includes URL for the Hollis Image record, Harvard Digital Collections record, and the CURIOSity record |
| permission       | Copyright statement and usage                                                                          |
| author           | It refers to the producer of the currency                                                              |
| description      | A description of the condition of the physical object                                                  |
| date             | Some items only include an approximate decade or century                                               |
| medium           | Physical description of the object and the materials used to create it                                 |
| dimensions       | measurements of the object in centimeters                                                              |
| institution      | A Wikidata tag referring to an institution within Harvard University                                   |
| department       | A Wikidata tag referring to a library at Harvard University                                            |
| notes            | The recommended citation format to aid the user                                                        |
| accession number | MH:VIA tag                                                                                             |



### Rights Statements

![Rights statement workflow](../leclouse/Rights_Statement_Workflow.svg)

For more information about the rights statement and resources utilized in the project, please go [here](https://gitlab.com/hldsi/WikiMedia_Currency_Collection/-/blob/master/leclouse/README.md).

## Packages Used

The packages used in this program were:

| Package           | Usage                                                                 |
| ----------------- | --------------------------------------------------------------------- |
| requests          | To interact with the Harvard LibraryCloud API.  |
| sleep from time   | To handle errors when communicating with the Harvard LibraryCloud API. |
| hashlib           | To create SHA1 Checksums for Wikimedia. |
| csv               | To create and manage CSV files. |
| os | To create and manage directories on local computer and restore images from local storage. |
| bs4.BeautifulSoup | To handle and search XML files.       |
| imghdr | To detect correct image file type. |

We used Pattypan to handle bulk upload of images to Wikimedia Commons with their associated metadata. It is important to note here that Wikimedia has rate limits for the quantity of bulk uploads; this limit will depend on your user status in the Wikimedia community. ([See more](https://commons.wikimedia.org/wiki/Commons:Guide_to_batch_uploading#Rate_limits))


## Program Description

### Version 1.0 OLD VERSION (Access Harvard LibraryCloud API using JSON)

This program allows you to request information from the Harvard LibraryCloud API using a URN.
With the URN, the program will request and save the metadata of the item as dictionary in a cache file.
The program is testing the HTTPS connection, requesting the URL and Deep Link of the digital resource (image).
With the Deep Link, the program is generating a SHA1 and downloading the image binary to a local directory in the computer.

![Data workflow](./Wikimedia_Program.svg)

This program is not fully updated because during developement we discovered that the Harvard LibraryCloud API was easier to parse using an XML dom. Further implementations focus on implementing class methods instead of independent functions.

### Version 2.0 CURRENT VERSION (Access Harvard LibraryCloud API using XML)

This program allows you to request information from the Harvard LibraryCloud API using a URN. With the URN, the program will request and save the metadata of the item as XML in a cache file. The program will access the XML cache file using XML minidom and request an image binary that will be saved in local computer, it will also calculate SHA1 of the saved file and it will discover the deep link of it.

The program will generate a metadata CSV spreadsheet that includes local directory, title, source, permission, author, description, date, medium, dimensions, institution, department, notes, and accesion number, deep link and sha1.

The class structure allows the program to set up a standard of what information is required to upload in Wikimedia and how to parse the Harvard LibraryCloud API metadata for this purpose. As for example the HarvardItem class only inludes functions that requests item URNs, URLs, deep links, title, media binary, sha1, the URLs are used to create the source field. For Wikimedia it is required to include the permission statement, title, source, and the media at the moment of upload.

The CurrencyItem is a child class to the HarvardItem Class. This class can be modified depending on the project. For the American Currency project we used the [WikiMedia artwork template](https://commons.wikimedia.org/wiki/Template:Artwork) to focus the data parsing and to generate the spreadsheet that is going to be used with Pattypan.


#

# How to use

## Prepare your environment

Download required additional libraries, requests and beautifulsoup4.
```
pip install -U requests beautifulsoup4
```

Suggestion: Use a python environment.

#

## Set script global variables

Change the variable in the next table inside `getting_data.py`.

|Variable| Description | Example Value |
|--------|-----------|-------|
| `OUTPUT_FILE` | Program will output organize metadata into this CSV | "pattypan_ready_data.csv" |
| `SAVE_DIR` | Absolute path to local image storage directory. | "/path/to/image/storage/dir/" |
| `DSI_JSTOR_CSV` | Path to local JSTOR File. Specific to DSI Currency project. | "American Currency collection_money-JSTOR.csv"|
| `LOCAL_HLC_API_CACHE` | Path to local Harvard LibraryCloud API metadata cache | "dsi_cache_xml.json" |
| `WIKIMEDIA_CATEGORIES` | Variable to input the Wikimedia categories of the project. Categories are separated by `;` no spaces. | "Harvard University Library;Harvard CURIOSity Collections;American Currency" |
| `PERMISION_STATEMENT` | Variable to input the permission statements of the collection |

Paths should use the running platform convention. 

#### Windows 

Use `\` for folder hierarchy. Absolute path should begin with partition letter like "C:\\path\\to\\folder\\"

#### Linux and Macs 
Use `/` for directory hierarchy. Absolute paths should begin with `/`.

#

## Execute

1. Clone the 'Wikimedia_Currency_Collection directory in your computer. 
`git clone https://gitlab.com/hldsi/WikiMedia_Currency_Collection.git`

2. Look up the `irmafrat` subdirectory, open the file `getting_data.py` and modify the global variables explained before.

3. Open your terminal or command prompt or powershell. 

4. Verify that you have Python3. If not, install it. (Same with PIP and beautifulSoup)
- [More information about installing Python3](https://realpython.com/installing-python/)
- [More information about installing PIP](https://pip.pypa.io/en/stable/installing/)
- [More information about installing BeautifulSoup](https://www.geeksforgeeks.org/beautifulsoup-installation-python/#:~:text=To%20install%20Beautifulsoup%20on%20Windows,PIP%20Installation%20%E2%80%93%20Windows%20%7C%7C%20Linux.&text=Wait%20and%20relax%2C%20Beautifulsoup%20would%20be%20installed%20shortly.)

5. In your terminal or command prompt or powershell, go to the directory were the project was cloned and subdirectory `irmafrat`. 
- (i.e Microsoft:       `cd C:\Documents\Wikimedia_Currency_Collection\irmafrat`)
- (i.e Linux/Macs:      `cd /Documents/WikiMedia_Currency_Collection/irmafrat`)

6. Run the script `getting_data.py` with `python`.


As the program runs it will display a counter, the filepath of the suggested item image storage, the actual filepath including the extension and the image extension.

- File name makes use of Harvard LibraryCloud API URN but replaces the `':'` with `'_'`.

- Output will be located at the file set in global variable `OUTPUT_FILE`. 

### Pattypan output file preparation

The output file is in CSV format and it should be converted into an XLS (Excel 97-2003). A second tab named "Template" needs to hold the entire Pattypan template in the box A1.

#### Pattypan template

[Pattypan](https://commons.wikimedia.org/wiki/Commons:Pattypan) is used to validate and upload the images along with the metadata. This program offers templates to help arrange the data and add categories. We used only the validation and upload components of Pattypan because our Python programs already generate the structured metadata that Pattypan needs for bulk uploading. [Read more.](https://commons.wikimedia.org/wiki/Commons:Pattypan/Simple_manual)

The following is the template used for this project.

```
'=={{int:filedesc}}==
{{Artwork
 |artist =
 |author = ${author}
 |title = ${title}
 |description = ${description}
 |date = ${date}
 |medium = ${medium}
 |dimensions = ${dimensions}
 |institution = ${institution}
 |department = ${department}
 |place of discovery =
 |object history =
 |exhibition history =
 |credit line =
 |inscriptions = ${inscriptions}
 |notes = ${notes}
 |accession number = ${accession_number}
 |place of creation =
 |source = ${source}
 |permission = ${permission}
 |other_versions =
 |references =
 |wikidata =
}}

=={{int:license-header}}==
{{PD-US}}


<#if categories ? has_content>
<#list categories ? split(";") as category>
[[Category:${category?trim}]]
</#list>
<#else>{{subst:unc}}
</#if>
```

### Run it for other collections

#### For other developers

The methods used inside the classes to extract MODS data from the Harvard LibraryCloud API can probably be used in many other projects. Feel free to adapt them to your needs.

The program is extracting SHA1 which is used in Wikimedia to verify with the [MediaWiki API](https://www.mediawiki.org/wiki/API:Main_page) if the item you are about to upload exists. [See more](https://commons.wikimedia.org/wiki/Commons:Guide_to_batch_uploading#Process)

Also the program is extracting the deep link of the image. This URL can be used to upload the image without having to download them locally ([see more](https://commons.wikimedia.org/wiki/Commons:Upload_tools#Upload_by_URL)). It is needed to ask Wikimedia Commons permision and to be [whitelisted](https://commons.wikimedia.org/wiki/Commons:Upload_tools/wgCopyUploadsDomains). 

# Further development

- Auto generate Wikimedia metadata templates.
- Create methods and/or functions to directly upload images without the need of Pattypan. (Need to create a [bot](https://en.wikipedia.org/wiki/Wikipedia:Bots) in Wikimedia)
- Generate a GUI for this interface.

## Suggestion for Next Step: Google Analytics integration for tracking Wikimedia impact

### Objective: 
Creating a workflow that combines the Wikimedia REST API and Google Analytics. The Wikimedia REST API allows you to retrieve the pageviews of a wiki site. Google Analytics allows you to track user access patterns, meaning that the Harvard Google Analytics platform can add a Wikimedia tracking link. This tracking link can be added for CURIOsity Collections, Harvard Digital Collections, and HOLLIS Images, which are the links we are using in the source metadata field in Wikimedia Commons. Thus, those links are the most likely ones that Wikimedia users will use to learn more about various objects and collections on Harvard websites.

### Methods: 

I. There is a Wikimedia REST API that allows the user to request information about the page views of a project. The REST API allows you to request information about pageviews in multiple formats: for individual pages (meaning each object page) and for the global of a project (meaning the total pageviews for the items in the collection). 

Impact:
    
    1. I do not know if this means an extra step to the Wikimedia upload meaning that we need to find out how we can define our project in Wikimedia so it can be retrievable by the API as a collection instead of individual images. 
    2. The upload has to happen first!
    3. We have to develop Python code for this.

II. Using Google Analytics to track user access patterns. Google Analytics allows you to track the page where the user was before accessing the Harvard ones (CURIOsity, Harvard Digital Collections, and HOLLIS Images) 

Impact:

    1. This can mean that we need to talk/meet with professionals in the office working with the Harvard Library Google Analytics, see how it works and how we can propose this idea. 

### Metrics: 
With the Wikimedia REST API numbers and the Google Analytics numbers we can have statistics on the users who use the Harvard Wikimedia collections and how many of them access the Harvard sites -- CURIOsity, Harvard Digital Collections, and Hollis Images -- from the corresponding Wikimedia pages.

This way the institution can measure the impact of Harvard Collections published on Wikimedia. This can allow DSI to promote usage of Harvard libraries and archives via migration of CURIOSity collections to Wikimedia.  

Also, in combination with Google Analytics, DSI and Harvard can visualize the user traffic from Wikimedia to Harvard Library platforms.

## Suggestion for Next Step: Consider integrating archival finding aids with Wikidata

This suggestion is tentative and dependent on whether any data exists on whether such a project would
improve the visibility of given archival collections at Harvard.

Below are two web resources to spur thinking and conversation in the future:

[Wikidata Archival Description Project](https://www.wikidata.org/wiki/Wikidata:WikiProject_Archival_Description)

Excerpt from Wikidata Archival Description Project page linked above on August 6, 2020: 

"The aim of the present project is to create the world’s most comprehensive high quality database of archival fonds and heritage collections, to represent archival structures within Wikidata where this is deemed useful and to ensure the interlinking between archival finding aids and Wikidata."

["Wikidata and Archivists" presentation by Katrina Cohen-Palacios at the Archives Association of Ontario Institutional Issues Forum, October 24, 2019](https://yorkspace.library.yorku.ca/xmlui/bitstream/handle/10315/36898/AAOIIF%202019-Wikidata-archivists-with-notes.pdf?sequence=1&isAllowed=y)

A nicely prepared resource for learning more, including slides and notes. Suitable as an introduction after reading Wikidata page.

## Suggestion for Next Step: Consider adding  a Wikidata item for the American Currency Collection as a whole

Harvard Library staff might consider adding such a [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page) item as an infobox on the [American Currency Collection category page.](https://commons.wikimedia.org/wiki/Category:American_Currency_Collection)